// Fill out your copyright notice in the Description page of Project Settings.

#include "MyBallorangeTrap.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "PreTPI_ShieldHeroCharacter.h"
#include "PaperSpriteComponent.h"
#include "Engine.h"


//----- Constructeur de la classe (configurations par d�faut) -----// 
AMyBallorangeTrap::AMyBallorangeTrap()
{
	//----- Cr�ation et configuration de la capsule de collision -----// 
	UCapsuleComponent* CollisionComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CollisionComp"));
	CollisionComp->InitCapsuleSize(41.0f, 1.0f);
	RootComponent = CollisionComp;
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	//----- Cr�ation et ajout de l'image du pi�ge -----// 
	BallorangeSprite = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("Ballorange Sprite"));
	BallorangeSprite->SetCollisionProfileName(TEXT("CollComp"));
	BallorangeSprite->SetupAttachment(RootComponent);
}
