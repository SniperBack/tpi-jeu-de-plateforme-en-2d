// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "GameDataClass.generated.h"


//----- Nom de la classe : UGameDataClass
//----- Description : 
//-----  Cette classe va servir � gestion du stockage des donn�es en local dans le jeu
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
UCLASS()
class PRETPI_SHIELDHERO_API UGameDataClass : public UGameInstance
{
	GENERATED_BODY()

	//----- Constructeur de la classe (configurations par d�faut) -----// 
public:
	UGameDataClass(const FObjectInitializer& ObjectInitializer);

	//----- Variable contenant le pseudo du joueur -----//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = FString)
	FString Pseudo;

	//----- Variable contenant le num�ro du niveau jou� -----//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = FString)
	int NumeroNiveau;

	//----- Variable contenant l'ID du joueur -----//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = int)
	int IDJoueur;

	//----- Variable contenant l'argent du joueur -----//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = int)
	int Argent;

	//----- Variable contenant la statistique du temps que le joueur a pass� en jeu -----//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = float)
	float TempsEnJeu;

	//----- Variable contenant la statistique des ragequits -----//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = int)
	int Ragequit;

	//----- Variable contenant la statistique du nombre de morts -----//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = int)
	int NbrMorts;

	//----- Variable contenant la statistique de l'argent gagn� en tout par le joueur -----//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = int)
	int ArgentGagne;

	//----- Variable contenant la statistique de l'argent d�pens� en tout par le joueur -----//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = int)
	int ArgentDepense;

	//----- Variable indiquant si le Power-Up "Double Pi�ces" est activ� -----//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = bool)
	bool IsDoublePiece;
};
