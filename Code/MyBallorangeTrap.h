// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyTrap.h"
#include "MyBallorangeTrap.generated.h"

//----- Nom de la classe : AMyBallorangeTrap
//----- Description : 
//-----  Cette classe va servir � la gestion du pi�ge "ballorange" dans le jeu. Elle va h�riter de la classe "AMyTrap"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

UCLASS()
class PRETPI_SHIELDHERO_API AMyBallorangeTrap : public AMyTrap
{
	GENERATED_BODY()

public:
	//----- Constructeur de la classe (configurations par d�faut) -----// 
	AMyBallorangeTrap();

	//----- Variable qui va contenir l'image du pi�ge -----// 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sprites")
	class UPaperSpriteComponent* BallorangeSprite;

};
