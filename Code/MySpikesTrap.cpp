// Fill out your copyright notice in the Description page of Project Settings.

#include "MySpikesTrap.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "PreTPI_ShieldHeroCharacter.h"
#include "PaperSpriteComponent.h"
#include "Engine.h"

//----- Constructeur de la classe (configurations par d�faut) -----// 
AMySpikesTrap::AMySpikesTrap()
{
	//----- Cr�ation et configuration de la capsule de collision -----// 
	UCapsuleComponent* CollisionComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CollisionComp"));
	CollisionComp->InitCapsuleSize(39.0f, 1.0f);
	RootComponent = CollisionComp;
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	//----- Cr�ation et ajout de l'image du pi�ge -----// 
	SpikeSprite = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("Spike Sprite"));
	SpikeSprite->SetCollisionProfileName(TEXT("CollComp"));
	SpikeSprite->SetupAttachment(RootComponent);
}
