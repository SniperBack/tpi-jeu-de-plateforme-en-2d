

// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "PreTPI_ShieldHeroCharacter.h"
#include "PaperFlipbookComponent.h"
#include "Components/TextRenderComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "Camera/CameraComponent.h"
#include "Blueprint/UserWidget.h"
#include "MyBallorangeTrap.h"
#include "GameDataClass.h"
#include "Engine.h"

DEFINE_LOG_CATEGORY_STATIC(SideScrollerCharacter, Log, All);

APreTPI_ShieldHeroCharacter::APreTPI_ShieldHeroCharacter()
{
	//----- Utilise seulement les mouvements YAW du personnage -----//
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;

	//----- Definition de la taille de la caspule de collision -----//
	GetCapsuleComponent()->SetCapsuleHalfHeight(67.0f);
	GetCapsuleComponent()->SetCapsuleRadius(21.0f);

	//----- Creation d'un bras virtuel qui sera attach� � la racine du personnage (capsule de collision) -----//
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 500.0f;
	CameraBoom->SocketOffset = FVector(0.0f, 0.0f, 75.0f);
	CameraBoom->bAbsoluteRotation = true;
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->RelativeRotation = FRotator(0.0f, -90.0f, 0.0f);
	

	//----- Creation d'une cam�ra qui sera attach�e � l'autre c�t� du bras virtuel -----//
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SideViewCamera"));
	CameraComponent->ProjectionMode = ECameraProjectionMode::Orthographic;
	CameraComponent->OrthoWidth = 1500.0f;
	CameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);

	//----- D�sactivation des rotations de la cam�ra, du bras et du personnage -----//
	CameraBoom->bAbsoluteRotation = true;
	CameraComponent->bUsePawnControlRotation = false;
	CameraComponent->bAutoActivate = true;
	GetCharacterMovement()->bOrientRotationToMovement = false;

	//----- Configuration des mouvements du personnage -----//
	GetCharacterMovement()->GravityScale = 2.7f;
	GetCharacterMovement()->AirControl = 1.0f;
	GetCharacterMovement()->JumpZVelocity = 1100.f;
	GetCharacterMovement()->GroundFriction = 3.0f;
	GetCharacterMovement()->MaxWalkSpeed = 600.0f;
	GetCharacterMovement()->MaxFlySpeed = 600.0f;

	//----- Bloquage du mouvement du personnage sur un seul axe -----//
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->SetPlaneConstraintNormal(FVector(0.0f, -1.0f, 0.0f));
	GetCharacterMovement()->bUseFlatBaseForFloorChecks = true;

	//----- Mise � z�ro des variables de v�rification -----//
	isinvincible = false;
	islevelfinished = false;
}

void APreTPI_ShieldHeroCharacter::UpdateAnimation()
{	
	//----- Cr�ation de variable pour stocker et v�rifier la vitesse du personnage -----//
	const FVector PlayerVelocity = GetVelocity();
	const float PlayerSpeedSqr = PlayerVelocity.SizeSquared();

	//----- V�rification de la vitesse du personnage pour voir si il est en mouvement ou immobile -----//
	UPaperFlipbook* DesiredAnimation = (PlayerSpeedSqr > 0.0f) ? Naofumi_Run : Naofumi_Idle;

	//----- Si il est mort, lance l'animation de mort -----//
	if (isDead) {
		GetSprite()->SetFlipbook(Naofumi_Death);
	} 
	//----- Sinon, si il est en train de tomber, lance l'animation de chute -----//
	else if (GetCharacterMovement()->IsFalling()) {
		GetSprite()->SetFlipbook(Naofumi_Jump);
	}
	//----- Sinon, lance l'animation imobile ou en mouvement par rapport � ce qu'on � v�rifi� au-dessus -----//
	else if (GetSprite()->GetFlipbook() != DesiredAnimation) {
		GetSprite()->SetFlipbook(DesiredAnimation);
	}
}

void APreTPI_ShieldHeroCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	
	//----- Appel de la fonction "UpdateCharacter" -----//
	UpdateCharacter();	

}

void APreTPI_ShieldHeroCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	//----- Relie les touches configur�es � une fonction -----//
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("Menu", IE_Pressed, this, &APreTPI_ShieldHeroCharacter::ShowMenuInGame);
	PlayerInputComponent->BindAxis("MoveRight", this, &APreTPI_ShieldHeroCharacter::MoveRight);
}

void APreTPI_ShieldHeroCharacter::ShowMenuInGame()
{
	//----- Affiche le menu � l'�cran -----//
	MyInGameMenu->AddToViewport();
}

void APreTPI_ShieldHeroCharacter::MoveRight(float Value)
{
	//----- Rajoute la valeur de la touche au mouvement du personnage  -----//
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), Value);
}

void APreTPI_ShieldHeroCharacter::UpdateCharacter()
{
	//----- Appel de la fonction "UpdateAnimation" -----//
	UpdateAnimation();

	//----- Met � jour la rotation du joueur d�pendant de la v�locit� -----//
	const FVector PlayerVelocity = GetVelocity();	
	float TravelDirection = PlayerVelocity.X;

	//----- Met � jour la rotation du personnage -----//
	if (Controller != nullptr)
	{
		if (TravelDirection < 0.0f)
		{
			Controller->SetControlRotation(FRotator(0.0, 180.0f, 0.0f));
		}
		else if (TravelDirection > 0.0f)
		{
			Controller->SetControlRotation(FRotator(0.0f, 0.0f, 0.0f));
		}
	}
}

void APreTPI_ShieldHeroCharacter::Death() {
	//----- Regarde la valeur de la variable "isDead" pour �viter de le d�clencher plusieurs fois � la suite -----//
	if (!isDead) {
		//----- Cr�e un minuteur qui appelera une fonction � la fin -----//
		FTimerHandle DeathTimerHandle;
		GetWorld()->GetTimerManager().SetTimer(DeathTimerHandle, this, &APreTPI_ShieldHeroCharacter::Respawn, 0.6f, false);

		//----- Arr�te les mouvements du joueur et d�sactive les touches -----//
		GetMovementComponent()->StopMovementImmediately();
		APreTPI_ShieldHeroCharacter::DisableInput(GetWorld()->GetFirstPlayerController());

		//----- Appelle la classe stockant les donn�es et met � jour le nombre de morts -----//
		UGameDataClass* Instance = Cast<UGameDataClass>(UGameplayStatics::GetGameInstance(GetWorld()));
		if (Instance) {
			Instance->NbrMorts++;
		}
	}

	//----- Passe la variable "isDead" � true -----//
	isDead = true;
}

void APreTPI_ShieldHeroCharacter::Respawn() {
	//----- Passe la variable "isDead" � false -----//
	isDead = false;

	//----- Rends invisible le joueur le temps de le t�l�porter au point de spawn -----//
	SetActorHiddenInGame(true);
	SetActorLocation(SpawnPoint);
	SetActorHiddenInGame(false);

	//----- R�active les touches du joueur -----//
	APreTPI_ShieldHeroCharacter::EnableInput(GetWorld()->GetFirstPlayerController());
}

void APreTPI_ShieldHeroCharacter::LevelFinished() {

	//----- Affiche le menu de victoire � l'�cran, t�l�porte le joueur � son point de spawn et affiche le curseur -----//
	
	if (!islevelfinished) {
		islevelfinished = true;
		MyVictoryMenu->AddToViewport();
		GetWorld()->GetFirstPlayerController()->bShowMouseCursor = true;
	}

}

void APreTPI_ShieldHeroCharacter::ActivateInvincible() {

	//----- Cr�e un minuteur qui appelera une fonction � la fin -----//
	FTimerHandle InvincibleTimerHandle;
	GetWorld()->GetTimerManager().SetTimer(InvincibleTimerHandle, this, &APreTPI_ShieldHeroCharacter::DesactivateInvincible, 5.0f, false);

	//----- Passe la variable "isinvincible" � true -----//
	isinvincible = true;
}

void APreTPI_ShieldHeroCharacter::DesactivateInvincible() {

	//----- Passe la variable "isinvincible" � false -----//
	isinvincible = false;
}