// Fill out your copyright notice in the Description page of Project Settings.


#include "MyCoins.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "PaperSpriteComponent.h"
#include "GameDataClass.h"
#include "Engine.h"

//----- Constructeur de la classe (configurations par d�faut) -----//
AMyCoins::AMyCoins()
{
	//----- Activation de la fonction "Tick" -----//
	PrimaryActorTick.bCanEverTick = true;

	//----- Cr�ation et configuration de la capsule de collision -----// 
	UCapsuleComponent* CollisionComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CollisionComp"));
	CollisionComp->InitCapsuleSize(220.0f, 1.0f);
	RootComponent = CollisionComp;
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	//----- Cr�ation et ajout de l'image du pi�ge -----// 
	CoinSprite = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("Shield Sprite"));
	CoinSprite->SetCollisionProfileName(TEXT("CollComp"));
	CoinSprite->SetupAttachment(RootComponent);

	//----- Va chercher le son stock� dans le fichier Sounds et stocke sa r�f�rence -----//
	static ConstructorHelpers::FObjectFinder<USoundCue> CueFinder(
		TEXT("'/Game/2DNaofumi/Sounds/Coin_Cue.Coin_Cue'")
	);

	//----- R�cup�re le son de la r�f�rence et le stocke -----//
	CoinCue = CueFinder.Object;

	//----- Cr�e un composant qui va nous permettre la gestion du son -----//
	audioComponent = CreateDefaultSubobject<UAudioComponent>(
		TEXT("PropellerAudioComp")
		);

	//----- D�sactive le lancement du son � sa cr�ation -----//
	audioComponent->bAutoActivate = false;

	//----- V�rifie que le son n'est pas vide et configure le composant pour qu'il contienne le son r�cup�r� au-dessus -----//
	if (CoinCue->IsValidLowLevelFast()) {
		audioComponent->SetSound(CoinCue);
	}

}

//----- Fonction appel�e lorsqu'un joueur rentre dans la capsule de collision -----//
void AMyCoins::NotifyActorBeginOverlap(class AActor* Other)
{
	Super::NotifyActorBeginOverlap(Other);


	//----- Appel de la classe stockant les donn�es et ajout de la valeure de la pi�ce � l'argent du joueur -----//
	UGameDataClass* Instance = Cast<UGameDataClass>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (Instance){
		if (Instance->IsDoublePiece)
			ValeurePiece = ValeurePiece * 2;

		Instance->Argent = Instance->Argent + ValeurePiece;
		Instance->ArgentGagne = Instance->ArgentGagne + ValeurePiece;
	}

	//----- Joue le son de mort -----//
	audioComponent->Play();

	//----- Rend la pi�ce invisible -----//
	SetActorHiddenInGame(true);

	//----- Lancement d'un timer qui supprimera la pi�ce du monde � la fin du temps -----//
	FTimerHandle destroyTimerHandle;
	GetWorld()->GetTimerManager().SetTimer(destroyTimerHandle, this, &AMyCoins::DestroyActor, 0.5f, false);

}

//----- Fonction utilis�e pour supprimer la pi�ce du jeu -----//
void AMyCoins::DestroyActor() {
	Destroy();
}

//----- Fonction appel�e au d�marrage du jeu o� au spawn de l'acteur -----// 
void AMyCoins::BeginPlay()
{
	Super::BeginPlay();
}

//----- Fonction appel�e plusieurs fois par seconde (tout les tick) -----//
void AMyCoins::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//----- Va modifier � chaque tick la position de l'acteur par rapport au temps, ce qui va permettre de le faire flotter -----//
	FVector NewLocation = GetActorLocation();
	FRotator NewRotation = GetActorRotation();
	float DeltaHeight = (FMath::Sin(RunningTime + DeltaTime) - FMath::Sin(RunningTime));
	NewLocation.Z += DeltaHeight * 20.0f;
	NewRotation.Yaw += 1.0f;
	RunningTime += DeltaTime;
	SetActorLocation(NewLocation);
	SetActorRotation(NewRotation);

}

