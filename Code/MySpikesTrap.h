// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyTrap.h"
#include "MySpikesTrap.generated.h"

//----- Nom de la classe : AMySpikesTrap
//----- Description : 
//-----  Cette classe va servir � la gestion du pi�ge "spikes" dans le jeu. Elle va h�riter de la classe "AMyTrap"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
UCLASS()
class PRETPI_SHIELDHERO_API AMySpikesTrap : public AMyTrap
{
	GENERATED_BODY()

public:
	//----- Constructeur de la classe (configurations par d�faut) -----// 
	AMySpikesTrap();

	//----- Variable qui va contenir l'image du pi�ge -----// 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sprites")
	class UPaperSpriteComponent* SpikeSprite;
	
};
