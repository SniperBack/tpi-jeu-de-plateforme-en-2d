// Fill out your copyright notice in the Description page of Project Settings.

#include "MyTrap.h"
#include "Components/SphereComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "PreTPI_ShieldHeroCharacter.h"
#include "Engine.h"

//----- Constructeur de la classe (configurations par d�faut) -----// 
AMyTrap::AMyTrap()
{
 	//----- Activation de la fonction "Tick" -----//
	PrimaryActorTick.bCanEverTick = true;

	//----- Va chercher le son stock� dans le fichier Sounds et stocke sa r�f�rence -----//
	static ConstructorHelpers::FObjectFinder<USoundCue> CueFinder(
		TEXT("'/Game/2DNaofumi/Sounds/sword_Cue.sword_Cue'")
	);

	//----- R�cup�re le son de la r�f�rence et le stocke -----//
	swordCue = CueFinder.Object;

	//----- Cr�e un composant qui va nous permettre la gestion du son -----//
	audioComponent = CreateDefaultSubobject<UAudioComponent>(
		TEXT("PropellerAudioComp")
		);

	//----- D�sactive le lancement du son � sa cr�ation -----//
	audioComponent->bAutoActivate = false;

	//----- V�rifie que le son n'est pas vide et configure le composant pour qu'il contienne le son r�cup�r� au-dessus -----//
	if (swordCue->IsValidLowLevelFast()) {
		audioComponent->SetSound(swordCue);
	}
}

//----- Fonction appel�e au d�marrage du jeu o� au spawn de l'acteur -----// 
void AMyTrap::BeginPlay()
{
	Super::BeginPlay();
}

//----- Fonction appel�e plusieurs fois par seconde (tout les tick) -----// 
void AMyTrap::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

//----- Fonction appel�e lorsqu'un joueur rentre dans la capsule de collision -----// 
void AMyTrap::NotifyActorBeginOverlap(class AActor* Other)
{
	Super::NotifyActorBeginOverlap(Other);

	//----- R�cup�re tout les acteurs du jeu provenant de la classe "APreTPI_ShieldHeroCharacter". Vu qu'on n'a qu'un seul joueur, il va r�cup�rer celui que l'on veut -----//
	for (TActorIterator<APreTPI_ShieldHeroCharacter> actorItr(GetWorld()); actorItr; ++actorItr) {
		//----- Appel la fonction "Death" du personnage -----//
		if (!actorItr->isinvincible) {
			actorItr->Death();

			//----- Joue le son de mort -----//
			audioComponent->Play();
		}
	}

}