// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyBallorangeTrap.h"
#include "MyMovingBallorangeTrap.generated.h"

//----- Nom de la classe : AMyMovingBallorangeTrap
//----- Description : 
//-----  Cette classe va servir � la gestion d'un pi�ge en mouvement
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
UCLASS()
class PRETPI_SHIELDHERO_API AMyMovingBallorangeTrap : public AMyBallorangeTrap
{
	GENERATED_BODY()

public:
	//----- Constructeur de la classe (configurations par d�faut) -----// 
	AMyMovingBallorangeTrap();

public:
	//----- Fonction appel�e plusieurs fois par seconde (tout les tick) -----// 
	virtual void Tick(float DeltaTime) override;

	//----- Variable allant contenir la hauteur du pi�ge et aider � certains calculs -----//
	float height;

	//----- Variable allant permettre � inverser la vitesse de saut pour faire retomber le pi�ge -----//
	bool speedinversion;
};
