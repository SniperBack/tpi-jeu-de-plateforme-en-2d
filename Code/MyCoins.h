// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyCoins.generated.h"

//----- Nom de la classe : AMyCoins
//----- Description : 
//-----  Cette classe va servir � la gestion des pi�ces dans les niveaux du jeu
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

UCLASS()
class PRETPI_SHIELDHERO_API AMyCoins : public AActor
{
	GENERATED_BODY()

public:
	//----- Constructeur de la classe (configurations par d�faut) -----//
	AMyCoins();

protected:
	//----- Fonction appel�e au d�marrage du jeu o� au spawn de l'acteur -----// 
	virtual void BeginPlay() override;

public:
	//----- Fonction appel�e plusieurs fois par seconde (tout les tick) -----// 
	virtual void Tick(float DeltaTime) override;

	//----- Variable qui va servir au � contenir le temps en cours -----// 
	float RunningTime;

	//----- Fonction appel�e lorsqu'un joueur rentre dans la capsule de collision -----//
	virtual void NotifyActorBeginOverlap(class AActor* Other);

	//----- Fonction utilis�e pour supprimer la pi�ce du jeu -----//
	void DestroyActor();

	//----- Variable qui va contenir l'image du pi�ge -----// 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sprites")
	class UPaperSpriteComponent* CoinSprite;

	//----- Variable servant � la gestion de la valeure de la pi�ce -----//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = int)
	int ValeurePiece;

	//----- Cr�ation de deux variables qui vont �tre utilis�e pour la gestion du son -----//
	class USoundCue* CoinCue;
	class UAudioComponent* audioComponent;

};
