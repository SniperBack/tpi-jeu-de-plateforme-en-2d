// Fill out your copyright notice in the Description page of Project Settings.


#include "GameDataClass.h"

UGameDataClass::UGameDataClass(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

	// Mise � z�ro des variables au d�marage du jeu
	Pseudo = "";
	IDJoueur = NULL;
	Argent = 0;
	NumeroNiveau = NULL;
	CodeToExecute = "";
	TempsEnJeu = 0;
	Ragequit = 0;
	NbrMorts = 0;
	ArgentGagne = 0;
	ArgentDepense = 0;
	IsDoublePiece = false;
}