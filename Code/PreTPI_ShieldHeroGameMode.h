// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PreTPI_ShieldHeroGameMode.generated.h"

//----- Nom de la classe : APreTPI_ShieldHeroGameMode
//----- Description : 
//-----  Cette classe va d�finir "les lois" du jeu. Comme par exemple, quelle classe va �tre la classe jou�e par le joueur
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

UCLASS(minimalapi)
class APreTPI_ShieldHeroGameMode : public AGameModeBase
{
	GENERATED_BODY()

	//----- Constructeur de la classe (configurations par d�faut) -----// 
public:
	APreTPI_ShieldHeroGameMode();
};
