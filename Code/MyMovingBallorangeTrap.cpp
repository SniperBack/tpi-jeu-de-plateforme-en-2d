// Fill out your copyright notice in the Description page of Project Settings.


#include "MyMovingBallorangeTrap.h"
#include "Engine.h"

AMyMovingBallorangeTrap::AMyMovingBallorangeTrap()
{
	// Mise � z�ro des variables au d�marage du niveau
	height = 0.0f;
	speedinversion = false;
}

//----- Fonction appel�e plusieurs fois par seconde (tout les tick) -----// 
void AMyMovingBallorangeTrap::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//----- R�cup�ration de la position actuelle du pi�ge -----//
	FVector NewLocation = GetActorLocation();

	//----- Calculs de la vitesse et de la hauteur du saut du pi�ge par rapport � sa position et vitesse actuelle -----//
	if (!speedinversion) {
		height = height + 5;
		if (height == 55.0f) {
			speedinversion = true;
			NewLocation.Z = NewLocation.Z + height * 0.1f;
		}
	}
	else {
		height--;
		NewLocation.Z = NewLocation.Z + height * 0.1f;
		if (height == -55.0f)
			speedinversion = false;
	}

	//----- Mise � jour de la position du pi�ge -----//
	SetActorLocation(NewLocation);
}