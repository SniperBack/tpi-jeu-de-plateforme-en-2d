// Fill out your copyright notice in the Description page of Project Settings.

#include "MyLevelObjective.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "PreTPI_ShieldHeroCharacter.h"
#include "PaperSpriteComponent.h"
#include "Engine.h"

// Sets default values
AMyLevelObjective::AMyLevelObjective()
{
	//----- Activation de la fonction "Tick" -----//
	PrimaryActorTick.bCanEverTick = true;

	//----- Cr�ation et configuration de la capsule de collision -----// 
	UCapsuleComponent* CollisionComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CollisionComp"));
	CollisionComp->InitCapsuleSize(60.0f, 1.0f);
	RootComponent = CollisionComp;
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	//----- Cr�ation et ajout de l'image du pi�ge -----// 
	ShieldSprite = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("Shield Sprite"));
	ShieldSprite->SetCollisionProfileName(TEXT("CollComp"));
	ShieldSprite->SetupAttachment(RootComponent);

}

//----- Fonction appel�e lorsqu'un joueur rentre dans la capsule de collision -----//
void AMyLevelObjective::NotifyActorBeginOverlap(class AActor* Other)
{
	Super::NotifyActorBeginOverlap(Other);

	//----- R�cup�re tout les acteurs du jeu provenant de la classe "APreTPI_ShieldHeroCharacter". Vu qu'on n'a qu'un seul joueur, il va r�cup�rer celui que l'on veut -----//
	for (TActorIterator<APreTPI_ShieldHeroCharacter> actorItr(GetWorld()); actorItr; ++actorItr) {
		//----- Appel la fonction "LevelFinished" du personnage -----//
		actorItr->LevelFinished();
	}
}

//----- Fonction appel�e au d�marrage du jeu o� au spawn de l'acteur -----// 
void AMyLevelObjective::BeginPlay()
{
	Super::BeginPlay();
	
}

//----- Fonction appel�e plusieurs fois par seconde (tout les tick) -----// 
void AMyLevelObjective::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//----- Va modifier � chaque tick la position de l'acteur par rapport au temps, ce qui va permettre de le faire flotter -----//
	FVector NewLocation = GetActorLocation();
	float DeltaHeight = (FMath::Sin(RunningTime + DeltaTime) - FMath::Sin(RunningTime));
	NewLocation.Z += DeltaHeight * 20.0f;
	RunningTime += DeltaTime;
	SetActorLocation(NewLocation);
}

