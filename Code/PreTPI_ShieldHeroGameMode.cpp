// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "PreTPI_ShieldHeroGameMode.h"
#include "PreTPI_ShieldHeroCharacter.h"

//----- Constructeur de la classe (configurations par d�faut) -----// 
APreTPI_ShieldHeroGameMode::APreTPI_ShieldHeroGameMode()
{
	//----- D�fini la classe par d�faut jou�e par le joueur -----// 
	DefaultPawnClass = APreTPI_ShieldHeroCharacter::StaticClass();	
}
