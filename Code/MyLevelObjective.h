// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyLevelObjective.generated.h"

//----- Nom de la classe : AMyLevelObjective
//----- Description : 
//-----  Cette classe va servir � la cr�ation de l'objectif du niveau
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

UCLASS()
class PRETPI_SHIELDHERO_API AMyLevelObjective : public AActor
{
	GENERATED_BODY()
	
public:
	//----- Constructeur de la classe (configurations par d�faut) -----// 
	AMyLevelObjective();

protected:
	//----- Fonction appel�e au d�marrage du jeu o� au spawn de l'acteur -----// 
	virtual void BeginPlay() override;

public:
	//----- Fonction appel�e plusieurs fois par seconde (tout les tick) -----// 
	virtual void Tick(float DeltaTime) override;

	//----- Variable qui va servir au � contenir le temps en cours -----// 
	float RunningTime;

	//----- Fonction appel�e lorsqu'un joueur rentre dans la capsule de collision -----//
	virtual void NotifyActorBeginOverlap(class AActor* Other);

	//----- Variable qui va contenir l'image du pi�ge -----// 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sprites")
		class UPaperSpriteComponent* ShieldSprite;

};
