// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyTrap.generated.h"

//----- Nom de la classe : AMyTrap
//----- Description : 
//-----  Cette classe va servir � la gestion de mes pi�ges dans le jeu. Elle va contenir tout les param�tres redondant de chaque pi�ge
//-----  Je vais l'utiliser comme classe parent de mes pi�ges. Mes pi�ges vont donc tous h�riter de cette classe.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

UCLASS()
class PRETPI_SHIELDHERO_API AMyTrap : public AActor
{
	GENERATED_BODY()
	
public:	
	//----- Constructeur de la classe (configurations par d�faut) -----// 
	AMyTrap();

protected:
	//----- Fonction appel�e au d�marrage du jeu o� au spawn de l'acteur -----// 
	virtual void BeginPlay() override;

public:	
	//----- Fonction appel�e plusieurs fois par seconde (tout les tick) -----// 
	virtual void Tick(float DeltaTime) override;

	//----- Fonction appel�e lorsqu'un joueur rentre dans la capsule de collision -----// 
	virtual void NotifyActorBeginOverlap(class AActor* Other);

	//----- Cr�ation de deux variables qui vont �tre utilis�e pour la gestion du son -----//
	class USoundCue* swordCue;
	class UAudioComponent* audioComponent;
};
