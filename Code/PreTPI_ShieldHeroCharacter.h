// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PaperCharacter.h"
#include "PreTPI_ShieldHeroCharacter.generated.h"

//----- Nom de la classe : APreTPI_ShieldHeroCharacter
//----- Description : 
//-----  Cette classe va servir � la cr�ation et � la configuration de notre personnage
//-----  Elle contiendra �galement la configuration de la cam�ra ainsi que des animations de notre personnage
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

UCLASS(config=Game)
class APreTPI_ShieldHeroCharacter : public APaperCharacter
{
	GENERATED_BODY()

	//----- Constructeur de la classe (configurations par d�faut) -----// 
public:
	APreTPI_ShieldHeroCharacter();

	//----- Cette variable va nous permettre de cr�er la cam�ra -----//
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera, meta=(AllowPrivateAccess="true"))
	class UCameraComponent* CameraComponent;

	//----- Cette variable va faire le lien entre la cam�ra et le joueur -----//
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	//----- Variable contenant l'animation de course -----//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Animations)
	class UPaperFlipbook* Naofumi_Run;

	//----- Variable contenant l'animation o� le personnage est immobile -----//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* Naofumi_Idle;

	//----- Variable contenant l'animation de saut -----//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* Naofumi_Jump;

	//----- Variable contenant l'animation de mort -----//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* Naofumi_Death;

	//----- Variable indiquant si le Power-Up d'invincibilit� est activ� -----//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = bool)
	bool isinvincible;

	//----- Variable indiquant si le personnage est mort ou pas -----//
	bool isDead;

	//----- Variable indiquant si le personnage a fini le niveau -----//
	bool islevelfinished;

	//----- Fonction pour mettre � jour l'animation du joueur -----//
	void UpdateAnimation();

	//----- Fonction pour mettre � jour les mouvements du joueur -----//
	void MoveRight(float Value);

	//----- Fonction pour mettre � jour les param�tres du joueur -----//
	void UpdateCharacter();

	//----- Fonction pour afficher le menu en partie -----//
	void ShowMenuInGame();

	//----- Fonction appel�e plusieurs fois par seconde (tout les tick) -----// 
	virtual void Tick(float DeltaSeconds) override;

	//----- Fonction qui va configurer les touches du joueur -----//
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	//----- Fonction qui va g�rer la mort du personnage -----// 
public:
	void Death();

	//----- Fonction qui va g�rer la r�apparition du personnage -----// 
	void Respawn();

	//----- Fonction qui va g�rer l'activation du Power-Up d'invincibilit� -----//
	UFUNCTION(BlueprintCallable, Category = "Invincible")
	void ActivateInvincible();

	//----- Fonction qui va g�rer la d�sactivation du Power-Up d'invincibilit� -----//
	void DesactivateInvincible();

	//----- Fonction qui va g�rer la r�ussite du niveau  -----// 
public:
	void LevelFinished();

	//----- Variable qui va contenir les coordonn�es du point de spawn -----// 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = variables)
	FVector SpawnPoint;

	//----- Variable qui va contenir une r�f�rence au menu de victoire -----// 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UserWidgets")
	UUserWidget* MyVictoryMenu;

	//----- Variable qui va contenir une r�f�rence au menu en partie -------//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UserWidgets")
	UUserWidget* MyInGameMenu;

	//----- Variable qui va contenir une r�f�rence au menu en partie -------//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UserWidgets")
	UUserWidget* MyHUD;

};
